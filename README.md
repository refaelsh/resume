# Resume

This repo holds an Asciidoctor file of my resume.
There is a CI/CD for deploying it to a statically
generated website hosted by GitLab,
[here](https://refaelsh.gitlab.io/resume/index.html).
If need be, just right click and choose Save As, and you will get a copy of
the HTML that can be distributed.
